<<<<<<< HEAD
This is a TCP UART bridge build on ESP-IDF

Goal was having a Wifi HomeMatic Sender Modul for FHEM, so some parameter are optimized for this.
Trying to run UART0 results in some trouble, reason may be the connection of USB Chip to this interface,
so I moved to UART1 with TX_Pin:10 and RX_PIN:9. Meanwhile more configurations are available.

ESP32 Chip:
DevKitC compatible.
I took a Himalaya Board
https://www.ebay.de/itm/272544725911

SPIFFS Support: 
Wifi SSID & PW config via image/config.txt. Watchout having Linux linebreak.

src:
Source Code for MSYS2 ESP_IDF Windows environment (e.g. mkspiffs.exe)

bin:
Include all images and scripts flashing the bridge from Windows Commandline without any development environment.
Executables were compiled by myself.
COM configurations within the cmd scripts may replaced to your demands. 

"git clone https://bitbucket.org/linuxpaul/esp32-uartbridge.git"
edit esp32-uartbridge\bin\image\config.txt, watchout having Linux linebreak
"cd esp32-uartbridge\bin"
"flash_image.cmd"
May be you may wait 10 sec. after reboot while SPIFFS is formatted (just the 1st time you flash this images )
"flash_config.cmd"

While further changing config.txt, just run flash_config.cmd.  

Links:
FHEM:
https://fhem.de/

Sender:
https://www.elv.de/homematic-funkmodul-fuer-raspberry-pi-bausatz.html

Wiki:
https://wiki.fhem.de/wiki/HM-MOD-RPI-PCB_HomeMatic_Funkmodul_f%C3%BCr_Raspberry_Pi

Documentation:
official ESP-IDF Docs @ https://esp-idf.readthedocs.io/en/latest/
Kolbans ESP32 Book @ https://leanpub.com/u/kolban

Help: 
Espressif https://www.esp32.com/index.php

:)
linuxpaul



=======
This is a TCP UART bridge build on ESP-IDF

Goal was having a Wifi HomeMatic Sender Modul for FHEM, so some parameter are optimized for this.
Trying to run UART0 results in some trouble, reason may be the connection of USB Chip to this interface,
so I moved to UART1 with TX_Pin:10 and RX_PIN:9.

ESP32 Chip:
DevKitC compatible.
I took a Himalaya Board
https://www.ebay.de/itm/272544725911

SPIFFS Support: 
Wifi SSID & PW config via image/config.txt. Watchout having Linux linebreak.
More params may available next time.

src:
Source Code for MSYS2 ESP_IDF Windows environment (e.g. mkspiffs.exe)

bin:
Include all images and scripts flashing the bridge from Windows Commandline without any development environment.
Executables were compiled by myself.

"git clone https://bitbucket.org/linuxpaul/esp32-uartbridge.git"
edit esp32-uartbridge\bin\image\config.txt, watchout having Linux linebreak
"cd esp32-uartbridge\bin"
"flash_image.cmd"
May be you may wait 10 sec. after reboot while SPIFFS is formatted (just the 1st time you flash this images )
"flash_config.cmd"

While further changing config.txt, just run flash_config.cmd.  

#### FIX #####
30.08.2018 there is an issue with wifi reconnect hopefully fixed

Links:
FHEM:
https://fhem.de/

Sender:
https://www.elv.de/homematic-funkmodul-fuer-raspberry-pi-bausatz.html

Wiki:
https://wiki.fhem.de/wiki/HM-MOD-RPI-PCB_HomeMatic_Funkmodul_f%C3%BCr_Raspberry_Pi

Documentation:
official ESP-IDF Docs @ https://esp-idf.readthedocs.io/en/latest/
Kolbans ESP32 Book @ https://leanpub.com/u/kolban

Help: 
Espressif https://www.esp32.com/index.php

:)
linuxpaul



>>>>>>> multitask
