#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "sdkconfig.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include "driver/uart.h"
#include "lwip/sockets.h"
#include "esp_spiffs.h"

#define WIFI_PW CONFIG_WIFI_PASSWORD
#define WIFI_SSID CONFIG_WIFI_SSID
#define PORT_NUMBER 23
#define EX_UART_NUM 0x1
#define BOUD_RATE 115200
#define DATA_BIT UART_DATA_8_BITS
#define STOP_BIT 0
#define PARITY_BIT 0
#define FLOW_CTRL 0
#define TXD_IO 10
#define RXD_IO 9
#define RTS_IO -1
#define CTS_IO -1
#define BUF_SIZE 128
#define GPIO_BLINK GPIO_NUM_2

static xQueueHandle uart_in_queue = NULL;
static xQueueHandle uart_out_queue = NULL;
static xQueueHandle size_in_queue = NULL;
static xQueueHandle size_out_queue = NULL;

static uint8_t wifi_ssid[32] = WIFI_SSID;
static uint8_t wifi_pw[64] = WIFI_PW;
static int port_num = (int)PORT_NUMBER;
static uart_port_t uart_num = (uart_port_t)EX_UART_NUM;
static int boud_rate = BOUD_RATE;
static int data_bit = DATA_BIT;
static int stop_bit = STOP_BIT;
static int parity_bit = PARITY_BIT;
static int flow_ctrl = FLOW_CTRL;
static int txd_io = TXD_IO;
static int rxd_io = RXD_IO;
static int rts_io = RTS_IO;
static int cts_io = CTS_IO;
static int buffer = BUF_SIZE;
static gpio_num_t blink_io = GPIO_BLINK;

static struct sockaddr_in serverAddress;
static struct sockaddr_in clientAddress;

static char tag[] = "socket_server";
static int sock = -1;
static int clientSock = 0;
static xTaskHandle* socketTaskHandle = NULL;


static uint8_t init_socket(void)
{
	int ret = 0;
	u32_t optlen = sizeof(ret);
	socklen_t clientAddressLength = sizeof(clientAddress);
	clientSock = accept(sock, (struct sockaddr *)&clientAddress, &clientAddressLength);
	if (clientSock < 1) {
		ESP_LOGE(tag, "NOT accept: %d %s", clientSock, strerror(errno));
		return 0;
	}
	getsockopt(clientSock, SOL_SOCKET, SO_ERROR, &ret, &optlen);
	ESP_LOGI(tag, "accept: %d %s ", clientSock, strerror(ret));
	return 1;
}

static void socket_Task(void *pvParameters)
{
    static const char *TAG = "Socket_TASK";
    esp_log_level_set(TAG, ESP_LOG_INFO);

	ssize_t socketRead;
	ssize_t socketWrite;

	int ret = 0;
	u32_t optlen = sizeof(ret);

    uint8_t* txdata = malloc(10*buffer+1);
    uint8_t* rxdata = malloc(10*buffer+1);
    size_t nbytes;

	uint8_t status;
	enum status{init,read,write};
	status = init;

	while (1) {

		switch(status) {
			case init:
				if(init_socket())
				{
					status = read;
				}
				break;
			case read:
				memset(txdata,'\0', 10*buffer+1);
				socketRead = recv(clientSock, txdata, 10*buffer,MSG_DONTWAIT);
				if (socketRead == 0) {
					getsockopt(clientSock, SOL_SOCKET, SO_ERROR, &ret, &optlen);
					ESP_LOGE(tag, "Socket Closed: %d %s", clientSock, strerror(ret));
					close(clientSock);
					status = init;
					break;
				}
				else if(socketRead < 1) {
					if(getsockopt(clientSock, SOL_SOCKET, SO_ERROR, &ret, &optlen)) {
						ESP_LOGE(tag, "Socket Closed: %d %s", clientSock, strerror(ret));
						close(clientSock);
						status = init;
						break;
					}
				}
				else {
					ESP_LOGI(TAG, "SOCKET data RECV: %s, bytes: %d, socket %d", txdata,socketRead,clientSock);
					xQueueSendToBack(uart_out_queue, (void *)txdata, 1 );
					xQueueSendToBack(size_out_queue, &socketRead, 1);
				}
				status = write;
				vTaskDelay(5 / portTICK_PERIOD_MS);
				break;
			case write:
				memset(rxdata,'\0', 10*buffer+1);
				if (xQueueReceive(size_in_queue, &nbytes, 1)) {
					xQueueReceive(uart_in_queue, rxdata, 1);
					socketWrite = send(clientSock, rxdata, nbytes, 0);
					if (socketWrite <= 0) {
						getsockopt(clientSock, SOL_SOCKET, SO_ERROR, &ret, &optlen);
						ESP_LOGE(tag, "Socket Closed: %d %s", clientSock, strerror(ret));
						close(clientSock);
						status = init;
						break;
					}
					else {
						ESP_LOGI(TAG, "SOCKET data SENT %s, bytes: %d, socket %d", rxdata, socketWrite,clientSock);
					}
				}
				status = read;
				vTaskDelay(5 / portTICK_PERIOD_MS);
				break;
		}
	}
	free(txdata);
	free(rxdata);
	vTaskDelete(NULL);
}

static void uart_Task(void *pvParameters)
{
    static const char *TAG = "UART_TASK";
    esp_log_level_set(TAG, ESP_LOG_INFO);

    uint8_t* txdata = malloc(10*buffer+1);
    uint8_t* rxdata = malloc(10*buffer+1);
    size_t nbytes;

	int uartWrite;
	int uartRead;

	uint8_t status;
	enum status{read,write};
	status = read;

	while (1) {

		switch(status) {

			case read:
				memset(rxdata,'\0', 10*buffer+1);
				uartRead = uart_read_bytes(uart_num, rxdata, 10*buffer, 100 / portTICK_PERIOD_MS);
				if(uartRead > 0) {
					ESP_LOGI(TAG, "UART data RECV %s, bytes: %d", rxdata,uartRead);
					xQueueSendToBack(uart_in_queue, (void *)rxdata, 1);
					xQueueSendToBack(size_in_queue, &uartRead, 1);
				}
				status = write;
				vTaskDelay(30 / portTICK_PERIOD_MS);
				break;

			case write:
				memset(txdata,'\0', 10*buffer+1);
				if (xQueueReceive(size_out_queue, &nbytes, 1)) {
					xQueueReceive(uart_out_queue, txdata, 1);
					uartWrite = uart_write_bytes(uart_num, (const char*) txdata, nbytes);
					ESP_LOGI(TAG, "UART data SENT: %s, bytes: %d", txdata,uartWrite);
				}
				status = read;
				vTaskDelay(30 / portTICK_PERIOD_MS);
				break;
		}

	}
	vTaskDelete(NULL);

}

static void tcp_sock_init(void)
{
	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock < 0) {
		ESP_LOGE(tag, "socket: %d %s", sock, strerror(errno));
		exit(1);
	}
	ESP_LOGI(tag, "socket created");
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddress.sin_port = htons(port_num);
	int rc  = bind(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress));

	if (rc < 0) {
		ESP_LOGE(tag, "bind: %d %s", rc, strerror(errno));
		exit(1);
	}
	ESP_LOGI(tag, "socket bound");
	rc = listen(sock,1);
	if (rc < 0) {
		ESP_LOGE(tag, "listen: %d %s", rc, strerror(errno));
		exit(1);
	}
	ESP_LOGI(tag, "socket listen");
	xTaskCreate(socket_Task, "socket_Task", 3000, NULL, 15, socketTaskHandle);
}


esp_err_t event_handler(void *ctx, system_event_t *event)
{
    static const char *TAG = "WIFI_Event";
    esp_log_level_set(TAG, ESP_LOG_INFO);

	switch (event->event_id){
		case SYSTEM_EVENT_STA_GOT_IP:
			ESP_LOGI(TAG, "Our IP address is " IPSTR "\n", IP2STR(&event->event_info.got_ip.ip_info.ip));
			ESP_LOGI(TAG, "Starting TCP Socket Init...");
			tcp_sock_init();
			break;
		case SYSTEM_EVENT_WIFI_READY:
			ESP_LOGI(TAG, "Wifi ready");
			break;
		case SYSTEM_EVENT_STA_START:
			ESP_LOGI(TAG, "Station started");
			break;
		case SYSTEM_EVENT_STA_CONNECTED:
			ESP_LOGI(TAG, "Station connected");
			break;
		case SYSTEM_EVENT_STA_DISCONNECTED:
				if(socketTaskHandle){
					vTaskDelete(socketTaskHandle);
				}
		        close(sock);
		        esp_wifi_connect();
		        break;
		default:
			ESP_LOGE(TAG, "Trouble connecting Station\n\t...probably config data failed");
			ESP_LOGE(TAG, "Wifi SSID: %s, %d",wifi_ssid,sizeof(wifi_ssid));
			ESP_LOGE(TAG, "Wifi PW: %s, %d",wifi_pw,sizeof(wifi_pw));
			break;
	}
	return ESP_OK;
}


static void wifi_init(void)
{
    ESP_ERROR_CHECK( esp_event_loop_init(event_handler, NULL) );
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
    ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
    wifi_config_t sta_config;
    memcpy(sta_config.sta.ssid,wifi_ssid,sizeof(wifi_ssid));
	memcpy(sta_config.sta.password,wifi_pw,sizeof(wifi_pw));
	sta_config.sta.bssid_set = false;
    ESP_ERROR_CHECK( esp_wifi_set_config(WIFI_IF_STA, &sta_config) );
    ESP_ERROR_CHECK( esp_wifi_start() );
    ESP_ERROR_CHECK( esp_wifi_connect() );
}


static void uart_init(void)
{
    uart_config_t uart_config = {
        .baud_rate = boud_rate,
        .data_bits = data_bit,
        .parity = parity_bit,
        .stop_bits = stop_bit,
        .flow_ctrl = flow_ctrl
    };
    uart_param_config(uart_num, &uart_config);
    uart_set_pin(uart_num, txd_io, rxd_io, rts_io, cts_io);
    uart_driver_install(uart_num, buffer * 2, buffer * 2, 20, NULL, 0);
	xTaskCreate(uart_Task, "uart_Task", 3000, NULL, 15, NULL);
}

static void spiffs_init(void)
{
    static const char *TAG = "...SPIFFS...";
    esp_log_level_set(TAG, ESP_LOG_INFO);

    ESP_LOGI(TAG, "Initializing SPIFFS");

    esp_vfs_spiffs_conf_t conf = {
      .base_path = "/spiffs",
      .partition_label = NULL,
      .max_files = 5,
      .format_if_mount_failed = true
    };

    esp_err_t ret = esp_vfs_spiffs_register(&conf);

    if (ret != ESP_OK) {
        if (ret == ESP_FAIL) {
            ESP_LOGE(TAG, "Failed to mount or format filesystem");
        } else if (ret == ESP_ERR_NOT_FOUND) {
            ESP_LOGE(TAG, "Failed to find SPIFFS partition");
        } else {
            ESP_LOGE(TAG, "Failed to initialize SPIFFS (%d)", ret);
        }
        return;
    }

    size_t total = 0, used = 0;
    ret = esp_spiffs_info(NULL, &total, &used);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Failed to get SPIFFS partition information");
    } else {
        ESP_LOGI(TAG, "Partition size: total: %d, used: %d", total, used);
    }

}

char *getConfigData(char *buf, char *key)
{
	char *conf;						// pointer for the beginning of "key"
	char *repl;						// pointer for the next EOL (\n)
	int beg = 0;					// Index in buf where key begins
	int rep = 0;					// Index in buf of next EOL
	int size = strlen(key)+1;		// length of key +1 (WIFI_SSID=)
	conf = strstr(buf,key);			// find key in buf
	if(conf == NULL) {return NULL;}
	beg = conf - buf;				// get start Index of key
	repl = strchr(buf + beg, '\n');	// find next EOL after key
	if(repl){
		rep = repl - buf;			// get Index of next EOL in buf
		buf[rep] = '\0';			// replace \n with \0
	}
	return conf + size;				// return pointer to next of key+1
}

void read_config(void)
{
    static const char *TAG = "config.txt";
    esp_log_level_set(TAG, ESP_LOG_INFO);
	FILE *fd = fopen("/spiffs/config.txt", "rb");
	if (fd == NULL) {
		ESP_LOGE("[read]", "fopen failed");
		ESP_LOGI(TAG, "running default param");
		return;
	}
	int res = 0;
	char *buf = (char*)malloc(512);
	char *tmp = (char*)malloc(512);
	char *param;
	bzero(buf,512);
	bzero(tmp,512);
	res = fread(buf, 1, 511, fd);
	fclose(fd);
	ESP_LOGI(TAG, " bytes read: %d", res);
	buf[res] = '\0';
	strcpy(tmp,buf);
	param = getConfigData(tmp,"WIFI_SSID");
	if(param){
		ESP_LOGI(TAG,"SSID %s", param);
		bzero(wifi_ssid,sizeof(wifi_ssid));
		memcpy(wifi_ssid,(uint8_t*)param,strlen(param));
	}
	strcpy(tmp,buf);
	param = getConfigData(tmp,"WIFI_PW");
	if(param){
		ESP_LOGI(TAG,"PW %s", param);
		bzero(wifi_pw,sizeof(wifi_pw));
		memcpy(wifi_pw,(uint8_t*)param,strlen(param));
	}
	strcpy(tmp,buf);
	param = getConfigData(tmp,"PORT_NUMBER");
	if(param){
		ESP_LOGI(TAG,"Port %s", param);
		port_num = atoi(param);
	}
	strcpy(tmp,buf);
	param = getConfigData(tmp,"UART_NUM");
	if(param){
		ESP_LOGI(TAG,"UART %s", param);
		uart_num = atoi(param);
	}
	strcpy(tmp,buf);
	param = getConfigData(tmp,"BOUD_RATE");
	if(param){
		ESP_LOGI(TAG,"BOUD %s", param);
		boud_rate = atoi(param);
	}
	strcpy(tmp,buf);
	param = getConfigData(tmp,"DATA_BIT");
	if(param){
		ESP_LOGI(TAG,"DATA BTIS %s", param);
		data_bit = atoi(param);
	}
	strcpy(tmp,buf);
	param = getConfigData(tmp,"STOP_BIT");
	if(param){
		ESP_LOGI(TAG,"STOP BITS %s", param);
		stop_bit = atoi(param);
	}
	strcpy(tmp,buf);
	param = getConfigData(tmp,"PARITY_BIT");
	if(param){
		ESP_LOGI(TAG,"PARITY BITS %s", param);
		parity_bit = atoi(param);
	}
	strcpy(tmp,buf);
	param = getConfigData(tmp,"FLOW_CTRL");
	if(param){
		ESP_LOGI(TAG,"FLOW CTRL %s", param);
		flow_ctrl = atoi(param);
	}
	strcpy(tmp,buf);
	param = getConfigData(tmp,"TXD_IO");
	if(param){
		ESP_LOGI(TAG,"TXD IO %s", param);
		txd_io = atoi(param);
	}
	strcpy(tmp,buf);
	param = getConfigData(tmp,"RXD_IO");
	if(param){
		ESP_LOGI(TAG,"RXD IO %s", param);
		rxd_io = atoi(param);
	}
	strcpy(tmp,buf);
	param = getConfigData(tmp,"RTS_IO");
	if(param){
		ESP_LOGI(TAG,"RTS IO %s", param);
		rts_io = atoi(param);
	}
	strcpy(tmp,buf);
	param = getConfigData(tmp,"CTS_IO");
	if(param){
		ESP_LOGI(TAG,"CTS IO %s", param);
		rts_io = atoi(param);
	}
	strcpy(tmp,buf);
	param = getConfigData(tmp,"GPIO_BLINK");
	if(param){
		ESP_LOGI(TAG,"GPIO Blink %s", param);
		blink_io = (gpio_num_t)(atoi(param));
	}
	free(buf);
	free(tmp);
	buf = 0;
	tmp = 0;
	param = 0;
}

void app_main(void)
{
 	uart_in_queue = xQueueCreate(10,(10*buffer+1));
 	uart_out_queue = xQueueCreate(10,(10*buffer+1));
 	size_in_queue = xQueueCreate(10,sizeof(size_t));
 	size_out_queue = xQueueCreate(10,sizeof(size_t));

	nvs_flash_init();
    spiffs_init();
    read_config();
    tcpip_adapter_init();
    wifi_init();
    uart_init();

    gpio_set_direction(blink_io, GPIO_MODE_OUTPUT);
    int level = 0;
    while (true) {
        gpio_set_level(blink_io, level);
        level = !level;
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

